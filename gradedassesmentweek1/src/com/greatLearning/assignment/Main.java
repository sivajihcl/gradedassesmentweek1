package com.greatLearning.assignment;

public class Main {

	public static void main(String[] args) {
		
		/* create the object of TechDepartment and use all the methods
		   departmentName
		   getTodaysWork
		   getWorkDeadline
		   isTodayAHoliday
		   doActivity-HrDepartment
		   getTechStackInformation-TechDepartment*/
		SuperDepartment sd = new SuperDepartment();
		
		String name = sd.departmentName();
		String work = sd.getTodaysWork();
		String deadline = sd.getWorkDeadline();
		String holiday = sd.isTodayAHoliday();
		System.out.println(name);
		System.out.println(work);
		System.out.println(deadline);
		System.out.println(holiday);
		System.out.println();
		
		AdminDepartment ad = new AdminDepartment();
		
		String namead = ad.departmentName();
		String workad = ad.getTodaysWork();
		String deadlinead = ad.getWorkDeadline();
		String holidayad = ad.isTodayAHoliday();
		System.out.println(namead);
		System.out.println(workad);
		System.out.println(deadlinead);
		System.out.println(holidayad);
		System.out.println();
		
		// create the object of HrDepartment and use all the methods 
		
		HrDepartment hd = new HrDepartment();
		

		String namehr = hd.departmentName();
		String workhr = hd.getTodaysWork();
		String deadlinehr = hd.getWorkDeadline();
		String holidayhr = hd.isTodayAHoliday();
		String activity = hd.doActivity();
		System.out.println(namehr);
		System.out.println(workhr);
		System.out.println(deadlinehr);
		System.out.println(activity);
		System.out.println(holidayhr);
		System.out.println();
		
		
		// create the object of TechDepartment and use all the methods 
		
		TechDepartment td = new TechDepartment();
		
		String nametd = td.departmentName();
		String worktd = td.getTodaysWork();
		String deadlinetd = td.getWorkDeadline();
		String holidaytd = td.isTodayAHoliday();
		String techstack = td.getTechStackInformation();
		System.out.println(nametd);
		System.out.println(worktd);
		System.out.println(deadlinetd);
		System.out.println(techstack);
		System.out.println(holidaytd);			

	}

}
